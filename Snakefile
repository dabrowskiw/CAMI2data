configfile: "configfile.yaml"

import CAMIdatasetutils as cd
import os

reffilename = os.path.basename(config["refbaseurl"])

bt2_ext = [".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"]

rule all:
  input: 
    "caseData/pathogendetection_S1_L001_R1_001.fastq.gz"

#### 1000 genomes dataset download, 1000 genomes reference download & preparation

rule download1000genomesBAMs:
  input:
  output:
    expand("{outdir}/{mapping}", outdir=config["1000genomesDir"], mapping=config["mappings"]),
    expand("{outdir}/{mapping}.bai", outdir=config["1000genomesDir"], mapping=config["mappings"])
  run:
    cd.download(output, config["baseurl"])

rule download1000genomesReference:
  input:
  output:
    temp(config["1000genomesRefDir"] + "/" + reffilename + ".fai"), 
    temp(config["1000genomesRefDir"] + "/" + reffilename  + ".gz")
  run:
    cd.downloadRef(config["1000genomesRefDir"], config["refbaseurl"])

rule unpackReference:
  input:
    config["1000genomesRefDir"] + "/" + reffilename  + ".gz"
  output:
    temp(config["1000genomesRefDir"] + "/" + reffilename)
  shell: "gunzip -q -k {input} || :" # The downloaded archive contains trailing NULLs, which leads to non-zero exit status of gunzip and rule failure, || : forces 0 return

rule buildReferenceIndex:
  input: 
    config["1000genomesRefDir"] + "/" + reffilename
  output:
    expand("{refdir}/index/{reffilename}{ext}", refdir=config["1000genomesRefDir"], reffilename=reffilename, ext=bt2_ext) 
  shell: "bowtie2-build {config[1000genomesRefDir]}/{reffilename} {config[1000genomesRefDir]}/index/{reffilename}"

#### Dataset preparation

rule unpackOriginalReads:
  input:
    "caseData/8_S8_L001_R1_001.fastq.gz", "caseData/8_S8_L001_R2_001.fastq.gz"
  output:
    "caseData/8_S8_L001_R1_001.fastq", "caseData/8_S8_L001_R2_001.fastq"
  shell: "gunzip {input}"

# Since reads in 1000 genomes sequencing are 100 bases, cut each reads into 3 100 base reads to make it harder to distinguish between original and replaced reads. 
# For the sake of simplicity, pretend that output reads are single-end (with cutting reads, simulating sensible read pairing becomes difficult)
rule cutOriginalReads:
  input: 
    "caseData/8_S8_L001_R1_001.fastq", "caseData/8_S8_L001_R2_001.fastq"
  output:
    temp("caseData/cut_S1_L001_R1_001.fastq"),
    temp("caseData/cut_S1_L001_R2_001.fastq")
  run:
    cd.cutReads(input, output, config["randomseed"])

rule mapOriginalReads:
  input:
    "caseData/cut_S1_L001_R1_001.fastq", 
    "caseData/cut_S1_L001_R2_001.fastq", 
    expand("{refdir}/index/{reffilename}{ext}", refdir=config["1000genomesRefDir"], reffilename=reffilename, ext=bt2_ext) # bowtie2 index files
  output:
    temp("caseData/mapped.sam"), 
  threads: 100
  shell:
    "bowtie2 -x {config[1000genomesRefDir]}/index/{reffilename} -1 caseData/cut_S1_L001_R1_001.fastq -2 caseData/cut_S1_L001_R2_001.fastq -S caseData/mapped.sam --no-unal -p {threads}"

rule replaceOriginalHumanReadsWith100genomesReads:
  input:
    "caseData/mapped.sam", 
    "caseData/cut_S1_L001_R1_001.fastq",
    "caseData/cut_S1_L001_R2_001.fastq",
    expand("{outdir}/{mapping}", outdir=config["1000genomesDir"], mapping=config["mappings"]),
    expand("{outdir}/{mapping}.bai", outdir=config["1000genomesDir"], mapping=config["mappings"])
  output:
    temp("caseData/mapped_replaced_R1.fastq"),
    temp("caseData/mapped_replaced_R2.fastq"),
    temp("caseData/unmapped_R1.fastq"),
    temp("caseData/unmapped_R2.fastq"),
  run:
    cd.replace(input[0], input[1], input[2], [x for x in input if x.endswith(".bam")], output[0], output[1], output[2], output[3], config["randomseed"])

rule combineUnmappedAndReplacedReads:
  input: 
    "caseData/mapped_replaced_R1.fastq",
    "caseData/mapped_replaced_R2.fastq",
    "caseData/unmapped_R1.fastq",
    "caseData/unmapped_R2.fastq",
  output:
    temp("caseData/pathogendetection_S1_L001_R1_001.fastq"),
    temp("caseData/pathogendetection_S1_L001_R2_001.fastq")
  run:
    cd.combineReads(input[0], input[1], input[2], input[3], output[0], output[1], config["randomseed"])

rule gzipReads:
  input: 
    "caseData/pathogendetection_S1_L001_R1_001.fastq",
    "caseData/pathogendetection_S1_L001_R2_001.fastq"
  output:
    "caseData/pathogendetection_S1_L001_R1_001.fastq.gz",
    "caseData/pathogendetection_S1_L001_R2_001.fastq.gz"
  shell:
    "gzip {input}"
