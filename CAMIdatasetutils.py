import os, sys, pysam
from random import choice, shuffle
import random

class Read:
  def __init__(self, seq, qual, name=None):
    self.seq = seq.strip()
    self.qual = qual.strip()
    self.mappos = -1
    self.contig = ""
    self.name = name

  def __str__(self):
    res = []
    if self.name is not None:
      if self.name[0] == "@":
        res = [self.name, "\n"]
      else:
        res = ["@" + self.name, "\n"]
    res += [self.seq, "\n+\n", self.qual]
    return "".join(res)


class ReadPair:
  def __init__(self, r1, r2):
    self.read1 = r1
    self.read2 = r2

  def is_sane(self):
    return len(self.read1.seq) == 100 and len(self.read1.qual) == len(self.read1.seq) and len(self.read2.seq) == 100 and len(self.read2.qual) == len(self.read2.seq)  


def download(filelist, baseurl):
  for f in filelist:
    basename = f.split("/")[1]
    downpath = baseurl + basename.split(".")[0] + "/alignment/" + basename
    os.system("wget {downpath} -O {outfile}".format(downpath=downpath, outfile=f))

def downloadRef(outdir, basedir):
  filename = basedir.split("/")[-1]
  os.system("wget {basedir}.fai -O {outdir}/{filename}.fai".format(basedir=basedir, outdir=outdir, filename=filename))
  os.system("wget {basedir}.gz -O {outdir}/{filename}.gz".format(basedir=basedir, outdir=outdir, filename=filename))

def cutReads(infiles, outfiles, rseed):
  random.seed(rseed)
  # Sets to store all unique xpos, ypos and tiles used in read names
  xpos_s = set()
  ypos_s = set()
  tiles_s = set()
  reads = set()
  used = set()
  header = "@M02885:21:000000000-ACB5W:1:"
  numread = 0
  with open(infiles[0], "r") as f1, open(infiles[1], "r") as f2:
    for line in f1:
      f2.readline()
      # Store used tile, xpos, ypos for later use
      dat = line.split(" ")[0].split(":")
      tiles_s.add(dat[4])
      xpos_s.add(dat[5])
      ypos_s.add(dat[6])
      seq1 = f1.readline()
      f1.readline()
      qual1 = f1.readline()
      try:
        seq2 = f2.readline()
        f2.readline()
        qual2 = f2.readline()
      except:
        print("ERROR: There seem to be fewer lines in " + infiles[1] + " than in " + infiles[2] + ", but there should be the same number of forward and reverse reads!")
      # Cut read into 3 100bp reads
      reads.add(ReadPair(Read(seq1[:100], qual1[:100]), Read(seq2[:100], qual2[:100])))
      reads.add(ReadPair(Read(seq1[100:200], qual1[100:200]), Read(seq2[100:200], qual2[100:200])))
      reads.add(ReadPair(Read(seq1[200:300], qual1[200:300]), Read(seq2[200:300], qual2[200:300])))
      numread += 1
      if numread % 10000 == 0:
        sys.stdout.write("Read " + str(numread) + " reads...\r")
        sys.stdout.flush()
  print("Read " + str(numread) + " reads -> generating " + str(numread*3) + " paired-end 100 bp reads.")
  # Convert sets into arrays for quicker random access
  xpos = list(xpos_s)
  ypos = list(ypos_s)
  tiles = list(tiles_s)
  numwritten = 0
  with open(outfiles[0], "w") as f1, open(outfiles[1], "w") as f2:
    for read in reads:
      if len(read.read1.seq) < 100 or len(read.read1.seq) < 100:
        continue
      if not read.is_sane:
        continue
      # Generate a random, not-yet-used combination of xpos, ypos and tile. This will stall at large read numbers but works in this case.
      name = choice(tiles) + ":" + choice(xpos) + ":" + choice(ypos)
      while name in used:
        name = choice(tiles) + ":" + choice(xpos) + ":" + choice(ypos)
      used.add(name)
      f1.write(header + name + " 1:N:0:8\n")
      f1.write(str(read.read1))
      f1.write("\n")
      f2.write(header + name + " 2:N:0:8\n")
      f2.write(str(read.read2))
      f2.write("\n")
      numwritten += 1
      if numwritten % 10000 == 0:
        sys.stdout.write("Wrote " + str(numwritten) + "/" + str(numread*3) + " reads...\r")
        sys.stdout.flush()
  print("Wrote " + str(numwritten) + " reads.                           ")

def revcomp(seq):
  rc = dict((("A", "T"), ("G", "C"), ("T", "A"), ("C", "G"), ("N", "N")))
  return "".join([rc[x] for x in seq[::-1]])

def replace(mapped, read1, read2, refbams, outfastq1, outfastq2, unmapped1, unmapped2, rseed):
  random.seed(rseed)
  print("Loading original reads...\n")
  reads = dict()
  numread = 0
  with open(read1, "r") as f1, open(read2, "r") as f2:
    for line in f1:
      name1 = line.strip()[1:]
      name2 = f2.readline().strip()[1:]
      seq1 = f1.readline()
      f1.readline()
      qual1 = f1.readline()
      try:
        seq2 = f2.readline()
        f2.readline()
        qual2 = f2.readline()
      except:
        print("ERROR: There seem to be fewer lines in " + infiles[1] + " than in " + infiles[2] + ", but there should be the same number of forward and reverse reads!")
      reads[name1.split(" ")[0]] = ReadPair(Read(seq1, qual1, name1), Read(seq2, qual2, name2))
      numread += 1
      if numread % 10000 == 0:
        sys.stdout.write("Read " + str(numread) + " original reads...\r")
        sys.stdout.flush()
  print("Read " + str(numread) + " original reads.         ")

  print("Loading mapped read pairs...")
  numread = 0
  with pysam.AlignmentFile(mapped, "r") as f:
    for read in f.fetch():
      if read.mate_is_unmapped:
        continue
      if len(read.reference_name.strip()) == 0:
        continue
      numread += 1
      name = read.query_name.strip()
      if read.is_read1:
        reads[name].read1.mappos = read.get_reference_positions()[0]
        reads[name].read1.contig = read.reference_name
      else:
        reads[name].read2.mappos = read.get_reference_positions()[0]
        reads[name].read2.contig = read.reference_name
      if numread % 10000 == 0:
        sys.stdout.write("Read " + str(numread) + " mapped reads...\r")
        sys.stdout.flush()

  print("Read " + str(numread) + " mapped reads.       ")

  print("Writing unmapped pairs...")
  mappedreads = [x for x in reads.values() if x.read1.mappos != -1]
  unmappedreads = [x for x in reads.values() if x.read1.mappos == -1 and x.read2.mappos == -1]
  numread = 0
  with open(unmapped1, "w") as f1, open(unmapped2, "w") as f2:
    for read in unmappedreads:
      numread += 1
      f1.write(str(read.read1))
      f1.write("\n")
      f2.write(str(read.read2))
      f2.write("\n")
      if numread % 10000 == 0:
        sys.stdout.write("Wrote " + str(numread) + " unmapped reads...\r")
        sys.stdout.flush()

  print("Wrote " + str(numread) + " unmapped reads.       ")

  bams = [pysam.AlignmentFile(x, "rb") for x in refbams]
  notfound = 0
  found = 0
  print("Replacing reads...")
  with open(outfastq1, "w") as outf1, open(outfastq2, "w") as outf2:
    processed = 0
    for read in mappedreads:
      processed += 1
      shuffle(bams)
      foundread = False
      for bam in bams:
        altreads = dict() 
        iter1 = bam.fetch(read.read1.contig, read.read1.mappos, read.read1.mappos+1)
        iter2 = bam.fetch(read.read1.contig, read.read2.mappos, read.read2.mappos+1)
        for x in iter1:
          if len(x.seq) >= 100:
            altreads[x.query_name.strip()] = ReadPair(Read(x.seq[:100], read.read1.qual, read.read1.name), None)
        for x in iter2:
          if len(x.seq) >= 100:
            if x.query_name in altreads:
              altreads[x.query_name.strip()].read2 = Read(revcomp(x.seq[:100]), read.read2.qual, read.read2.name)
        altpairs = [x for x in altreads.values() if x.read2 is not None]
        if len(altpairs) == 0:
          continue
        shuffle(altpairs)
        for altread in altpairs:
          if not read.is_sane():
            continue
          foundread = True
          found += 1
          outf1.write(str(altread.read1))
          outf1.write("\n")
          outf2.write(str(altread.read2))
          outf2.write("\n")
          break
        if foundread:
          break
      if not foundread:
        notfound += 1
      if processed % 10 == 0:
        sys.stdout.write("Processed " + str(processed) + "/" + str(len(mappedreads)) + " reads, replaced " + str(found) + ", discarded " + str(notfound) + "...\r")
        sys.stdout.flush()
        outf1.flush()
        outf2.flush()
    print("Processed " + str(processed) + " reads, replaced " + str(found) + ", discarded " + str(notfound) + ".           ")

def get_fastq_read(f):
  name = f.readline().strip()
  if "" == name:
    return None
  seq = f.readline().strip()
  f.readline()
  qual = f.readline().strip()
  return Read(seq, qual, name)

def combineReads(mapped1, mapped2, unmapped1, unmapped2, out1, out2, rseed):
  print("Reading mapped reads...")
  random.seed(rseed)
  reads = []
  with open(mapped1, "r") as m1, open(mapped2, "r") as m2:
    while True:
      rp = ReadPair(get_fastq_read(m1), get_fastq_read(m2))
      if rp.read1 is None:
        break
      reads += [rp]
      if len(reads) % 10000 == 0:
        sys.stdout.write("Read " + str(len(reads)) + " mapped reads...\r")
        sys.stdout.flush()
  print("Read " + str(len(reads)) + " mapped reads.          ")
  print("Reading unmapped reads...")
  with open(unmapped1, "r") as u1, open(unmapped2, "r") as u2: 
    while True:
      rp = ReadPair(get_fastq_read(u1), get_fastq_read(u2))
      if rp.read1 is None:
        break
      reads += [rp]
      if len(reads) % 10000 == 0:
        sys.stdout.write("Read " + str(len(reads)) + " total reads...\r")
        sys.stdout.flush()
  print("Read " + str(len(reads)) + " total reads.          ")
  print("Shuffling reads...")
  shuffle(reads)
  print("Writing " + out1 + "/" + out2 + "...")
  with open(out1, "w") as f1, open(out2, "w") as f2:
    for read in reads:
      f1.write(str(read.read1))
      f1.write("\n")
      f2.write(str(read.read2))
      f2.write("\n")

