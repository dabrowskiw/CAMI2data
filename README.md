# Anonymization of CAMI II Pathogen Detection Dataset 

These scripts anonymize the raw provided data for the CAMI II pathogen detection challenge. First, data from several runs from the 1000 genomes project is downloaded. Then, the raw provided data is mapped to the same reference as that used by the 1000 genomes project. Mapping reads are replaced by randomly chosen reads from one of the 1000 genomes files that map at the same location in the human genome.

Since the reads in the original data are 300 bp paired end and the 1000 genomes reads are 100 bp single end, each original read is cut into 3 100 bp reads (so a dataset with 300 bp PE reads is converted into one with three times as many 100 bp readpairs) prior to mapping. This is to stop receipients from distinguishing original and replaced reads based on the read length. When reads are replaced, only the 1000 genomes read sequence is used, and the quality values from the original read are used, also in order to make distinguishing original from replaced reads harder.

The process is visualized in the following graph:

![Anonymization process](dag.png)

The approach used here for replacing reads (take mapping readpair from SAM file, get all readpairs mapping at the same positions from a random 1000 genomes BAM, randomly choose one of these as replacement) is extremely slow (only around 10 reads per second on an SSD). However, as this is a cDNA library containing only about 80k human reads in total, it is sufficient. If the same method were to be used for a dataset containing more human reads, the method replace in CAMIdatasetutils.py would need to be reimplemented in a more efficient manner.

## Dependencies

Dependencies are listed in env/packages.yaml. The easiest way to satisfy them is to use conda:

```
conda env create -n CAMIdataset -f env/packages.yaml
source activate CAMIdataset
```

## Running

The original files must be in the subfolder caseData and must be called 8_S8_L001_R1_001.fastq and 8_S8_L001_R2_001.fastq. Then, the whole process can be started using snakemake (preferably after creating and activating a conda environment with all dependencies as shown above):

```
snakemake --cores <NumberOfCores>
```

The number of cores is only relevant for the mapping step.

## General information 

Information concerning the data downloaded from the 1000 genomes project is stored in configfile.yaml:

 * mappings: alignment files (1000 genomes samples, randomly selected from phase 3) from which reads will be taken from to replace human reads 
 * baseurl: the 1000 genomes ftp base URL
 * refbaseurl: base URL for 1000 genomes phase 3 reference download
 * 1000genomesdir, 1000genomesRefdir: Output directories for downloads and reference index

